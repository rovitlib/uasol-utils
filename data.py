import json
import numpy as np

with open('complete.json') as json_file:
	json_data = json.load(json_file)

# Variables

color_img = []
depth_img = []
timestamp = []
translation = []
rotation = []
m = []
latitude_dir = []
longitude_dir = []
longitude = []
latitude = []

# Extracting the data

for i in range(0, len(json_data['Data'])):
	color_img.append(json_data['Data'][i]['color_frame'])
	depth_img.append(json_data['Data'][i]['depth_frame'])
	timestamp.append(json_data['Data'][i]['timestamp'])
	translation.append(json_data['Data'][i]['translation'])
	rotation.append(json_data['Data'][i]['rotation'])
	m.append(np.array(json_data['Data'][i]['m']))
	latitude_dir.append(np.array(json_data['Data'][i]['latitude dir']))
	longitude_dir.append(np.array(json_data['Data'][i]['longitude dir']))
	latitude.append(np.array(json_data['Data'][i]['latitude']))
	longitude.append(np.array(json_data['Data'][i]['longitude']))

#print(len(latitude))
#print(len(latitude_dir))
#print(len(longitude))
#print(len(longitude_dir))

