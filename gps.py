import time, serial
from pynmea import nmea
from flask import Flask, current_app
from flask import jsonify
import uptime
import signal
import json
import sys

obj = None

def signal_handler(signal, frame):
	print('Exiting and saving log...')
	#obj.write(']}')
	#obj.close
	
	obj.write(']}')

	obj.close()

	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

obj = open('gps.json', 'w')
obj.write('{"Data" :')
obj.write('[')

port ='COM5'
ser = serial.Serial(port)
ser.baudrate = 4800
ser.timeout = 1
#ser.open()




gpgga = nmea.GPGGA()
while True:
	data = ser.readline()
	data = data.decode('latin-1')
	if data.startswith('$GPGGA'):
		##method for parsing the sentence
		gpgga.parse(data)
		lat,dir1,longi,dir2 = data.split(",")[2:6]
		#print('Latitud', lat, 'dir',dir1)
		#print('Longitud' , longi, 'dir' , dir2)
		#time_stamp = gpgga.timestamp
		uptime_test = uptime._uptime_windows()
		jasonData = json.dumps({'uptime' : uptime_test, 'latitude' : lat, 'lat_dir' : dir1, 'longitude': longi, 'long_dir' : dir2}, ensure_ascii=False)
		#print('Timestamp', time_stamp)
		obj.write(jasonData)
		obj.write(',')
		obj.write('\n')
