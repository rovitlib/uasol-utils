import json
import numpy as np

with open('gps.json') as fd:
     json_data = json.load(fd)
# Variables

uptime_now = []
latitude = []
longitude = []
latitude_dir = []
longitude_dir = []

# Extracting the data

for i in range(0, len(json_data['Data'])):
	uptime_now.append(json_data['Data'][i]['timestamp'])
	latitude.append(json_data['Data'][i]['latitude'])
	longitude.append(json_data['Data'][i]['longitude'])
	latitude_dir.append(json_data['Data'][i]['lat_dir'])
	longitude_dir.append(json_data['Data'][i]['long_dir'])


def dm_map(x):
    degrees = int(float(x)) // 100
    minutes = - degrees * 100 + (float(x))
    return degrees, minutes


degrees_lat = []
degrees_long = []
min_lat = []
min_long = []

import csv
Writer = csv.writer(open('./file.csv', 'w', newline='\n'))
Writer.writerow(['Name', 'Latitude', 'Longitude'])



for i in range(0, len(uptime_now)):

	if latitude[i] != "" and longitude[i] != "":
		print(latitude[i])
		degrees_lati, minutes_lati = dm_map(latitude[i])
		degrees_lat.append(degrees_lati)
		min_lat.append(minutes_lati)

		degrees_longi, minutes_longi = dm_map(longitude[i])
		degrees_long.append(degrees_longi)
		min_long.append(minutes_longi)

		numbers = list(range(1, len(degrees_lat) +1))

		Writer.writerow([numbers[i], str(degrees_lati)+ ' ' + str(minutes_lati) + str(latitude_dir[i]), str(degrees_longi) + ' ' + str(minutes_longi) + str(longitude_dir[i])])
