########################################################################
#
# SVO READER FOR ZED CAMERA FILES
#
# Input = .svo file
# Output = 
#			- Camera Parameters
#			- Color Image
#			- Depth Map
#			- Point Cloud
#			- Rotation vector
#			- Translation vectorm 
#			- m matrix
# 
#
########################################################################


import sys
import pyzed.camera as zcam
import pyzed.types as tp
import pyzed.core as core
import pyzed.defines as sl
import cv2
from flask import Flask, current_app
from flask import jsonify
import json
import numpy as np

import signal

logFile = None
obj = None

def signal_handler(signal, frame):
	print('Exiting and saving log...')
	logFile.close()
	obj.write(']}')
	obj.close()

	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


logFile = '/media/zurich/data4tb/DataSet/polival23jorge/log.txt'
logFile = open(logFile, 'w+')


obj = open('/media/zurich/data4tb/DataSet/polival23jorge/data.json', 'w')
obj.write('{"Data" :')
obj.write('[')


def main():

	if len(sys.argv) != 2:
		print("Please specify path to .svo file.")
		exit()

	filepath = sys.argv[1]
	print("Reading SVO file: {0}".format(filepath))

	init = zcam.PyInitParameters(svo_input_filename=filepath,svo_real_time_mode=False,camera_resolution=sl.PyRESOLUTION.PyRESOLUTION_HD2K, enable_right_side_measure=True, depth_mode=sl.PyDEPTH_MODE.PyDEPTH_MODE_ULTRA)
	cam = zcam.PyZEDCamera()
	status = cam.open(init)
	if status != tp.PyERROR_CODE.PySUCCESS:
		print(repr(status))
		exit()

	runtime = zcam.PyRuntimeParameters()
	runtime.sensing_mode = sl.PySENSING_MODE.PySENSING_MODE_STANDARD
	mat_left = core.PyMat()
	mat_right = core.PyMat()
	camera_pose = zcam.PyPose()
	mat_depth = core.PyMat()
	py_translation = core.PyTranslation()
	

	key = 115
	print_camera_information(cam)
	
	i = 0
	while i < cam.get_svo_number_of_frames():  # for 'q' key
		print('Frame ' + str(i) + ' de ' + str(cam.get_svo_number_of_frames()))
		err = cam.grab(runtime)

		cam.retrieve_image(mat_left, sl.PyVIEW.PyVIEW_LEFT)
		cam.retrieve_image(mat_right, sl.PyVIEW.PyVIEW_RIGHT)

		name = 'frame'+ str(i)+'_color'
		depth = 'frame'+str(i)+'_depth'
		timestamp = saving_image(cam, key, mat_left, '/media/zurich/data4tb/DataSet/polival23jorge/Images/img_left'+ str(i)+ '_color.png')
		timestamp_right = saving_image(cam, key, mat_right, '/media/zurich/data4tb/DataSet/polival23jorge/Images/img_right'+ str(i)+ '_color.png')


		saving_depth(cam, '/media/zurich/data4tb/DataSet/polival23jorge/Images/img_'+ str(i)+ '_depth.png')


		#cam.retrieve_image(mat_depth, sl.PyVIEW.PyVIEW_DEPTH_RIGHT)
		#timestamp_depth = saving_image(cam, key, mat_left, '/media/zurich/Maxtor/DataSet/EPS3/img/img_right'+ str(i)+ '_depth.png')

		tracking_state = cam.get_position(camera_pose)
		translation = None
		rotation = None
		pose_data = None

		#if tracking_state == sl.PyTRACKING_STATE.PyTRACKING_STATE_OK:
		rotation = camera_pose.get_rotation_vector()
		rx = rotation[0]
		ry = rotation[1]
		rz = rotation[2]

		translation = camera_pose.get_translation(py_translation)
		tx = translation.get()[0]
		ty = translation.get()[1]
		tz = translation.get()[2]

		translation = [tx, ty, tz]
		rotation = [rx, ry, rz]
		pose_data = camera_pose.pose_data(core.PyTransform())

		#print(pose_data)

		
		num_frames = cam.get_svo_number_of_frames()
		#if i == 1206:

		#saving_point_cloud(cam, '/media/zurich/Maxtor/DataSet/Final/aulario1/Images/img'+ str(i)+'pc')
		a = np.array(pose_data.m)
		m = a.tolist()	
		jasonData = json.dumps({'color_frame' : name, 'depth_frame' : depth, 'timestamp' : timestamp, 'translation' : translation, 'rotation' : rotation, 'm' : m}, ensure_ascii=False)

		obj.write(jasonData)
		if i == num_frames:
			obj.write('\n')
			obj.wirte(']}')

		else:
			obj.write(',')
			obj.write('\n')
		
		i+=1

	obj.write(']}')
	obj.close()
	cam.close()
	print("\nFINISH")


def print_camera_information(cam):
	while True:
		if True:
			print()

			logFile.write('---- CAMERA INFORMATION ----')
			logFile.write('\n')
			logFile.write('\n')

			logFile.write('RIGHT CAMERA PARAMETERS')
			logFile.write('\n')
			logFile.write("Optical center along x axis, defined in pixels: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.cx))
			logFile.write("Optical center along y axis, defined in pixels: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.cy))
			logFile.write('\n')
			logFile.write("Focal length in pixels alog x axis: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.fx))
			logFile.write("Focal length in pixels alog y axis: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.fy))
			logFile.write('\n')
			logFile.write("Vertical field of view after stereo rectification (in degrees): {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.v_fov))
			logFile.write("Horizontal field of view after stereo rectification (in degrees): {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.h_fov))
			logFile.write("Diagonal field of view after stereo rectification (in degrees): {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.d_fov))
			logFile.write('\n')
			logFile.write("Distorsion factor of the right cam before calibration: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.right_cam.disto))
			logFile.write('\n')
			logFile.write("Distorsion factor of the right cam after calibration: {0}.\n".format(
				cam.get_camera_information().calibration_parameters.right_cam.disto))
			logFile.write('\n')

			logFile.write('LEFT CAMERA PARAMETERS')
			logFile.write('\n')
			logFile.write("Optical center along x axis, defined in pixels: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.cx))
			logFile.write("Optical center along y axis, defined in pixels: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.cy))
			logFile.write('\n')
			logFile.write("Focal length in pixels alog x axis: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.fx))
			logFile.write("Focal length in pixels alog y axis: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.fy))
			logFile.write('\n')
			logFile.write("Vertical field of view after stereo rectification (in degrees): {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.v_fov))
			logFile.write("Horizontal field of view after stereo rectification (in degrees): {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.h_fov))
			logFile.write("Diagonal field of view after stereo rectification (in degrees): {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.d_fov))
			logFile.write('\n')
			logFile.write("Distorsion factor of the left cam before calibration: {0}.\n".format(
				cam.get_camera_information().calibration_parameters_raw.left_cam.disto))
			logFile.write('\n')
			logFile.write("Distorsion factor of the left cam after calibration: {0}.\n".format(
				cam.get_camera_information().calibration_parameters.left_cam.disto))
			logFile.write('\n')

			logFile.write("Confidence threshold: {0}.\n".format(cam.get_confidence_threshold()))
			logFile.write('\n')
			logFile.write("Depth min and max range values: {0}, {1}.\n".format(cam.get_depth_min_range_value(),
																	cam.get_depth_max_range_value()))
			logFile.write('\n')
			logFile.write("Resolution: {0}, {1}.\n".format(round(cam.get_resolution().width, 2), cam.get_resolution().height))
			logFile.write('\n')
			logFile.write("Camera FPS: {0}.\n".format(cam.get_camera_fps()))
			logFile.write('\n')
			logFile.write("Frame count: {0}.\n".format(cam.get_svo_number_of_frames()))

			break
	


def saving_image(cam, key, mat, name):
	if key == 115:
		img = tp.PyERROR_CODE.PyERROR_CODE_FAILURE
		while img != tp.PyERROR_CODE.PySUCCESS:
			timestamp = cam.get_camera_timestamp()
			filepath = name
			img = mat.write(filepath)
			#print('Timestamp: {0}\n'.format(timestamp))


	return timestamp


def saving_depth(cam, name):
	while True:
		if True:
			save_depth = 0
			while not save_depth:
				filepath = name
				save_depth = zcam.save_camera_depth_as(cam, sl.PyDEPTH_FORMAT.PyDEPTH_FORMAT_PNG, filepath)
				if save_depth:
					break
			break


def saving_point_cloud(cam, name):
	while True:
		if True:
			save_point_cloud = 0
			while not save_point_cloud:
				filepath = name
				save_point_cloud = zcam.save_camera_point_cloud_as(cam,
																   sl.PyPOINT_CLOUD_FORMAT.
																   PyPOINT_CLOUD_FORMAT_PCD_ASCII,
																   filepath, True)
				if save_point_cloud:
					break
			break
			

if __name__ == "__main__":
	main()
